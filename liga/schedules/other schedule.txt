FINAL SCHEDULE

4/9/19 - tuesday
rockets vs daks		- kidz basketball		40008
lasvillas vs spartans	- battleground basketball	30004
ducks vs phasev		- battleground basketball	30005
skeptron vs adlights	- elite basketball		20018

4/12/19 - Friday
nksmb vs thunders	- kidz basketball		40009
rockets vs gold		- kidz basketball		40010
itsybitsy vs phasev	- kidz volleyball		60003
teamawa vs regenerate   - elite volleyball		50007

4/13/19 - sat
lasvillas vs nksmb	- battleground basketball	30006
edp vs phasev		- senior basketball		10008
rave vs gold		- elite basketball		20016
riders vs redhorse	- elite basketball		20017
	
4/14/19 - sun
koc vs toda		- senior basketball		10009
knicks vs otso		- senior basketball		10010
18th vs adlights	- elite basketball		20015
nksmb vs gas		- elite basketball		20019

4/26/19 - Friday
rockets vs nksmb	- kidz basketball		40011
daks vs gold		- kidz basketball		40012
itsybitsy vs phasev	- kidz volleyball		60004
patateam vs teamawa	- elite volleyball		50008

4/27/19 - sat
phasev vs nksmb		- battleground basketball	30007
toda vs phasev		- senior basketball		10011
gold vs riders		- elite basketball		20020
18th vs nksmb		- elite basketball		20021

4/28/19 - sun
edp vs koc		- senior basketball		10012
knicks vs bha		- senior basketball		10013
adlights vs gas		- elite basketball		20022
redhorse vs rave	- elite basketball		20023

5/3/19 - Friday
thunders vs rockets	- kidz basketball		40013
nksmb vs daks		- kidz basketball		40014
victeam vs regenerate	- elite volleyball		50009

5/4/19 - sat
ducks vs lasvillas	- battleground basketball	30008	
knicks vs phasev	- senior basketball		10014
gold vs 18th		- elite basketball		20024
riders vs skeptron	- elite basketball		20025

5/5/19 - sun
edp vs toda		- senior basketball		10015
otso vs koc		- senior basketball		10016
redhorse vs gas		- elite basketball		20026
rave vs nksmb		- elite basketball		20027

5/10/19 - Friday
thunders vs nksmb	- kidz basketball		40016
gold vs rockets		- kidz basketball		40015
itsybitsy vs phasev	- kidz volleyball		60005
patateam vs victeam	- elite volleyball		50010

5/11/19 - sat
spartans vs nksmb	- battleground basketball	30009
koc vs phasev		- senior basketball		10017
riders vs adlights	- elite basketball		20028
skeptron vs rave	- elite basketball		20029

5/12/19 - sun
knicks vs edp		- senior basketball		10018
bha vs otso		- senior basketball		10019
gold vs gas		- elite basketball		20030
rave vs 18th		- elite basketball		20031

5/17/19 - Friday
daks vs thunders	- kidz basketball		40017
gold vs nksmb		- kidz basketball		40018	
teamawa vs regenerate	- elite volleyball		50011

5/18/19 - sat
ducks vs spartans	- battleground basketball	30010	
otso vs toda		- senior basketball		10021
riders vs nksmb		- elite basketball		20032
redhorse vs skeptron	- elite basketball		20033

5/19/19 - sun
bha vs phasev		- senior basketball		10020
gas vs skeptron		- elite basketball		20034
gold vs redhorse	- elite basketball		20035
adlights vs nksmb	- elite basketball		20036

5/24/19 - Friday
rockets vs daks		- kidz basketball		40019
gold vs thunders	- kidz basketball		40020
itsybitsy vs phasev	- kidz volleyball		60006
victeam vs teamawa 	- elite volleyball		50012


- SEMIS
5/25/19 - Saturday
1 game kid basketball
1 game battleground basketball
1 game senior basketball
1 game elite basketball

5/26/19 - Sunday
1 game kid basketball
1 game battleground basketball
1 game senior basketball
1 game elite basketball

- FINALS
June 1
Volleyball Kidz
Battleground
Senior

June 2
Basketball Kidz
Volleyball Elites
Basketball Elites


